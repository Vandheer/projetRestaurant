<?php

namespace SuperRestaurantBundle\Controller;

use SuperRestaurantBundle\Entity\Plat;
use SuperRestaurantBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Plat controller.
 *
 * @Route("plat")
 */
class PlatController extends Controller
{

    /**
     * Lists all plat entities.
     *
     * @Route("/", name="plat_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $plats = $em->getRepository('SuperRestaurantBundle:Plat')->findAll();
        } else {
            $plats = $em->getRepository('SuperRestaurantBundle:Plat')->findAllPublishedPlats();
        }

        return $this->render('plat/index.html.twig', array(
            'plats' => $plats,
        ));
    }

    /**
     * Creates a new plat entity.
     *
     * @Security("has_role('ROLE_EDITEUR')")
     *
     * @Route("/new", name="plat_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $plat = new Plat();
        $form = $this->createForm('SuperRestaurantBundle\Form\PlatType', $plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $plat->setStatut("En validation");

            $file = $plat->getImageUpload();

            if (isset($file)){
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('plats_directory'),
                    $fileName
                );
                $plat->setImage($fileName);
            }

            if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                throw $this->createAccessDeniedException();
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $plat->setUser($user);

            $em->persist($plat);
            $em->flush();

            $editeurs = $this->getDoctrine()->getRepository('SuperRestaurantBundle:User')->findAllEditeurs();
            $tabEditeurs = array();
            foreach ($editeurs as $e){
                array_push($tabEditeurs, $e['user_email']);
            }

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_REVIEWER')) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Hello')
                    ->setFrom('superrestaurantbundle@gmail.com')
                    ->setTo($tabEditeurs)
                    ->setBody(
                        $this->renderView(
                            'emails/registration_plat.html.twig',
                            array('user' => $user, 'plat' => $plat)
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Un e-mail a été envoyé.');
            }

            return $this->redirectToRoute('plat_show', array('id' => $plat->getId()));
        }

        return $this->render('plat/new.html.twig', array(
            'plat' => $plat,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a plat entity.
     *
     * @Route("/{id}", name="plat_show")
     * @Method("GET")
     */
    public function showAction(Plat $plat)
    {
        $deleteForm = $this->createDeleteForm($plat);

        return $this->render('plat/show.html.twig', array(
            'plat' => $plat,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing plat entity.
     *
     * @Security("has_role('ROLE_EDITEUR')")
     *
     * @Route("/{id}/edit", name="plat_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Plat $plat)
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();
        if($plat->getUser() != $user){
            throw new AccessDeniedException('Accès refusé.');
        }

        $editForm = $this->createForm('SuperRestaurantBundle\Form\PlatType', $plat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $file = $plat->getImageUpload();

            if (isset($file)){
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('plats_directory'),
                    $fileName
                );
                $plat->setImage($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('plat_show', array('id' => $plat->getId()));
        }

        return $this->render('plat/edit.html.twig', array(
            'plat' => $plat,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Validate a plat
     *
     * @Security("has_role('ROLE_REVIEWER')")
     *
     * @Route("/validate/{id}", name="plat_validate")
     * @Method("GET")
     */
    public function validateAction(Request $request, Plat $plat)
    {
        $em = $this->getDoctrine()->getManager();
        $plat->setStatut("Validé");
        $em->persist($plat);
        $em->flush();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $serveurs = $this->getDoctrine()->getRepository('SuperRestaurantBundle:User')->findAllServeurs();
        $tabServeurs = array();
        foreach ($serveurs as $e){
            array_push($tabServeurs, $e['user_email']);
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Hello')
            ->setFrom('superrestaurantbundle@gmail.com')
            ->setTo($tabServeurs)
            ->setBody(
                $this->renderView(
                    'emails/validation_plat.html.twig',
                    array('user' => $user, 'plat' => $plat)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);

        $request->getSession()->getFlashBag()->add('success', 'Un e-mail a été envoyé.');
        $request->getSession()->getFlashBag()->add('success', 'Le plat a bien été validé !');

        return $this->redirectToRoute('plat_show',array('id' => $plat->getId()));
    }

    /**
     * Refuse a plat
     *
     * @Security("has_role('ROLE_REVIEWER')")
     *
     * @Route("/refuse/{id}", name="plat_refuse")
     * @Method("GET")
     */
    public function refuseAction(Request $request, Plat $plat)
    {
        $em = $this->getDoctrine()->getManager();
        $plat->setStatut("Refusé");
        $em->persist($plat);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'Le plat a bien été refusé.');

        return $this->redirectToRoute('plat_show',array('id' => $plat->getId()));
    }

    /**
     * Deletes a plat entity.
     *
     * @Security("has_role('ROLE_CHEF')")
     *
     * @Route("/{id}", name="plat_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Plat $plat)
    {
        $form = $this->createDeleteForm($plat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($plat);
            $em->flush($plat);
        }

        return $this->redirectToRoute('plat_index');
    }

    /**
     * Creates a form to delete a plat entity.
     *
     * @param Plat $plat The plat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Plat $plat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plat_delete', array('id' => $plat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
