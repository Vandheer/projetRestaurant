<?php

namespace SuperRestaurantBundle\Controller;

use SuperRestaurantBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/admin", name="admin_add")
     * @Method("GET")
     */
    public function addAdminAction(){
        $editeur = new User();
        $editeur->setIsActive(true)
            ->setUsername('editeur')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setId(1)
            ->setEmail('editeur@gmail.com')
            ->setRoles(array('ROLE_EDITEUR'));
        $this->getDoctrine()->getManager()->persist($editeur);

        $serveur = new User();
        $serveur->setIsActive(true)
            ->setUsername('serveur')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setId(2)
            ->setEmail('serveur@gmail.com')
            ->setRoles(array('ROLE_SERVEUR'));
        $this->getDoctrine()->getManager()->persist($serveur);

        $reviewer = new User();
        $reviewer->setIsActive(true)
            ->setUsername('reviewer')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setId(3)
            ->setEmail('reviewer@gmail.com')
            ->setRoles(array('ROLE_REVIEWER'));
        $this->getDoctrine()->getManager()->persist($reviewer);

        $chef = new User();
        $chef->setIsActive(true)
            ->setUsername('chef')
            ->setPassword('$2y$12$g6SZY3bqve7WQlEm20jroePkTFEyGa1C6RHfKxN27V8iWMlJrWoi.')
            ->setId(4)
            ->setEmail('chef@gmail.com')
            ->setRoles(array('ROLE_CHEF'));
        $this->getDoctrine()->getManager()->persist($chef);

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('plat_index');
    }
}
