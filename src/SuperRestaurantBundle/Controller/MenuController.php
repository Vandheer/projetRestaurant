<?php

namespace SuperRestaurantBundle\Controller;

use SuperRestaurantBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Menu controller.
 *
 * @Route("menu")
 */
class MenuController extends Controller
{
    /**
     * Lists all menu entities.
     *
     * @Route("/", name="menu_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $menus = $em->getRepository('SuperRestaurantBundle:Menu')->findBy(array(), array('ordre' => 'ASC'));
        } else {
            $menus = $em->getRepository('SuperRestaurantBundle:Menu')->findAllPublishedMenus();
        }

        return $this->render('menu/index.html.twig', array(
            'menus' => $menus,
        ));
    }

    /**
     * Creates a new menu entity.
     *
     * @Security("has_role('ROLE_EDITEUR')")
     *
     * @Route("/new", name="menu_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $menu = new Menu();
        $form = $this->createForm('SuperRestaurantBundle\Form\MenuType', $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $menu->setStatut('En validation');
            $em->persist($menu);
            $em->flush($menu);

            $user = $this->get('security.token_storage')->getToken()->getUser();
            $menu->setUser($user);

            $em->persist($menu);
            $em->flush();

            $editeurs = $this->getDoctrine()->getRepository('SuperRestaurantBundle:User')->findAllEditeurs();
            $tabEditeurs = array();
            foreach ($editeurs as $e){
                array_push($tabEditeurs, $e['user_email']);
            }

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_REVIEWER')) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Hello')
                    ->setFrom('superrestaurantbundle@gmail.com')
                    ->setTo($tabEditeurs)
                    ->setBody(
                        $this->renderView(
                            'emails/registration_menu.html.twig',
                            array('user' => $user, 'menu' => $menu)
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Un e-mail a été envoyé.');
            }

            return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
        }

        return $this->render('menu/new.html.twig', array(
            'menu' => $menu,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a menu entity.
     *
     * @Route("/{id}", name="menu_show")
     * @Method("GET")
     */
    public function showAction(Menu $menu)
    {
        $deleteForm = $this->createDeleteForm($menu);

        return $this->render('menu/show.html.twig', array(
            'menu' => $menu,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing menu entity.
     *
     * @Security("has_role('ROLE_EDITEUR')")
     *
     * @Route("/{id}/edit", name="menu_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Menu $menu)
    {
        $user= $this->get('security.token_storage')->getToken()->getUser();
        if($menu->getUser() != $user){
            throw new AccessDeniedException('Accès refusé.');
        }

        $deleteForm = $this->createDeleteForm($menu);
        $editForm = $this->createForm('SuperRestaurantBundle\Form\MenuType', $menu);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('menu_show', array('id' => $menu->getId()));
        }

        return $this->render('menu/edit.html.twig', array(
            'menu' => $menu,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Validate a menu
     *
     * @Security("has_role('ROLE_REVIEWER')")
     *
     * @Route("/validate/{id}", name="menu_validate")
     * @Method("GET")
     */
    public function validateAction(Request $request, Menu $menu)
    {
        $em = $this->getDoctrine()->getManager();
        $menu->setStatut("Validé");
        $em->persist($menu);
        $em->flush();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $serveurs = $this->getDoctrine()->getRepository('SuperRestaurantBundle:User')->findAllServeurs();
        $tabServeurs = array();
        foreach ($serveurs as $e){
            array_push($tabServeurs, $e['user_email']);
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Hello')
            ->setFrom('superrestaurantbundle@gmail.com')
            ->setTo($tabServeurs)
            ->setBody(
                $this->renderView(
                    'emails/validation_menu.html.twig',
                    array('user' => $user, 'menu' => $menu)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);

        $request->getSession()->getFlashBag()->add('success', 'Un e-mail a été envoyé.');
        $request->getSession()->getFlashBag()->add('success', 'Le menu a bien été validé !');

        return $this->redirectToRoute('menu_show',array('id' => $menu->getId()));
    }

    /**
     * Refuse a menu
     *
     * @Security("has_role('ROLE_REVIEWER')")
     *
     * @Route("/refuse/{id}", name="menu_refuse")
     * @Method("GET")
     */
    public function refuseAction(Request $request, Menu $menu)
    {
        $em = $this->getDoctrine()->getManager();
        $menu->setStatut("Refusé");
        $em->persist($menu);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'Le menu a bien été refusé.');

        return $this->redirectToRoute('menu_show',array('id' => $menu->getId()));
    }

    /**
     * Deletes a menu entity.
     *
     * @Security("has_role('ROLE_CHEF')")
     *
     * @Route("/{id}", name="menu_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Menu $menu)
    {
        $form = $this->createDeleteForm($menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush($menu);
        }

        return $this->redirectToRoute('menu_index');
    }

    /**
     * Creates a form to delete a menu entity.
     *
     * @param Menu $menu The menu entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Menu $menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menu_delete', array('id' => $menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
