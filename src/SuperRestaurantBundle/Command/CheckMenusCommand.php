<?php
/**
 * Created by PhpStorm.
 * User: Joël
 * Date: 14/02/2017
 * Time: 13:54
 */

namespace SuperRestaurantBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckMenusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('SuperRestaurantBundle:check-menus')
            ->setDescription('Vérifie si les menus ont un plat')
            ->setHelp('Vérifie si les menus ont un plat et envoi un mail aux admins s\'il n\'en a pas.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $menus = $em->getRepository('SuperRestaurantBundle:Menu')->findAllWithoutPlat();

        if(!empty($menus)){
            $output->writeln([
                'Menus n\'ayant pas de plats',
                '==========================='
            ]);
            foreach ($menus as $menu){
                $output->writeln($menu->getTitre());
            }
            $output->writeln([
                '===========================',
                'Un mail a été envoyé aux chefs'
            ]);

            $chefs = $em->getRepository('SuperRestaurantBundle:User')->findAllChefs();
            $tabChefs = array();
            foreach ($chefs as $c){
                array_push($tabChefs, $c['user_email']);
            }

            $message = \Swift_Message::newInstance()
                ->setSubject('Hello')
                ->setFrom('superrestaurantbundle@gmail.com')
                ->setTo($tabChefs)
                ->setBody(
                    $this->getContainer()->get('templating')->render(
                        'emails/menulist.html.twig',
                        array('menus' => $menus)
                    ),
                    'text/html'
                );
            $this->getContainer()->get('mailer')->send($message);
        }else{
            $output->writeln('Tous les menus sont valides');
        }
    }
}