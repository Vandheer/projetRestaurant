<?php

namespace SuperRestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Plat
 *
 * @ORM\Table(name="plat")
 * @ORM\Entity(repositoryClass="SuperRestaurantBundle\Repository\PlatRepository")
 */
class Plat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Le prix doit être positif."
     * )
     * @Assert\NotBlank
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=255)
     */
    private $statut;

    /**
     * @var bool
     *
     * @ORM\Column(name="faitMaison", type="boolean")
     */
    private $faitMaison;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="plats")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @Assert\Image()
     */
    private $imageUpload;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="allergenes", type="text", nullable=true)
     */
    private $allergenes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Menu", mappedBy="plats", cascade={"persist", "remove"})
     */
    private $menus;

    public function __construct()
    {
        $this->menus = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Plat
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Plat
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Plat
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Plat
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set faitMaison
     *
     * @param boolean $faitMaison
     *
     * @return Plat
     */
    public function setFaitMaison($faitMaison)
    {
        $this->faitMaison = $faitMaison;

        return $this;
    }

    /**
     * Get faitMaison
     *
     * @return bool
     */
    public function getFaitMaison()
    {
        return $this->faitMaison;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Plat
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Plat
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Plat
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set allergenes
     *
     * @param string $allergenes
     *
     * @return Plat
     */
    public function setAllergenes($allergenes)
    {
        $this->allergenes = $allergenes;

        return $this;
    }

    /**
     * Get allergenes
     *
     * @return string
     */
    public function getAllergenes()
    {
        return $this->allergenes;
    }

    /**
     * @return string
     */
    public function getImageUpload()
    {
        return $this->imageUpload;
    }

    /**
     * @param $imageUpload
     * @return Plat
     */
    public function setImageUpload($imageUpload)
    {
        $this->imageUpload = $imageUpload;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenus()
    {
        return $this->menus;
    }

    /**
     * @param mixed $menus
     * @return Plat
     */
    public function setMenus($menus)
    {
        $this->menus = $menus;
        return $this;
    }

}

