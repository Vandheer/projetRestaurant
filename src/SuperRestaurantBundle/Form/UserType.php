<?php

namespace SuperRestaurantBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username')->add('password',PasswordType::class)->add('email')->add('isActive')
            ->add('roles',ChoiceType::class, array(
                'choices' => array(
                    'Serveur' => 'ROLE_SERVEUR',
                    'Editeur' => 'ROLE_EDITEUR',
                    'Reviewer' => 'ROLE_REVIEWER',
                    'Chef' => 'ROLE_CHEF',
                ),
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SuperRestaurantBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'superrestaurantbundle_user';
    }


}
